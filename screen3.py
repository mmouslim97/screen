#!/usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("Trop puissant")
window.geometry("800x600")

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)
third_frame = tk.Frame(window)

def first_screen():
    first_frame.pack()
    tk.Label(first_frame, text="Il y a une explication scientifique à tout, mais qui a créé la science ?").pack()

    button_next = tk.Button(first_frame, text="Next", command=second_screen)
    button_next.pack()

def second_screen():
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="Une paix fondée sur des morts, est-ce une paix ?").pack()

    button_next = tk.Button(second_frame, text="Next", command=lambda: third_screen(second_frame))
    button_next.pack()

def third_screen(previous_frame):
    previous_frame.destroy()
    third_frame.pack()
    tk.Label(third_frame, text="Les mauvais se voient comme des bon").pack()

first_screen()
window.mainloop()
