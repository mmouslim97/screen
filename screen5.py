#!usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("Santoryu Ogi")
window.geometry("800x600")

init_frame = tk.Frame(window)
init_frame.pack()

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)
third_frame = tk.Frame(window)
fourth_frame = {}
fifth_frame = []

def first_screen():
    first_frame.pack()
    tk.Label(first_frame, text="J'ai ma vision de la justice, et l'ennemi a la sienne, qui a raison ?").pack()

    button_next = tk.Button(first_frame, text="Next", command=second_screen)
    button_next.pack()

def second_screen():
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="De la lumière naissent les ténèbres, et des ténèbres la lumière").pack()

    button_next = tk.Button(second_frame, text='Next', command=lambda: third_screen(second_frame))
    button_next.pack()

def third_screen(previous_frame):
    previous_frame.destroy()
    third_frame.pack()
    tk.Label(third_frame, text="La paix ne ramène pas les morts de la guerre").pack()

    button_next = tk.Button(third_frame, text="Next", command=lambda: fourth_screen(third_frame))
    button_next.pack()

def fourth_screen(previous_frame):
    previous_frame.destroy()
    fourth_frame['f'] = tk.Frame(window)
    fourth_frame['f'].pack()
    mon_label = tk.Label(fourth_frame['f'], text="C'est pas le monde qui est sombre, ce sont les Hommes").pack()
    fourth_frame['tresor_cache'] = "3 btc"
    print(fourth_frame['tresor_cache'])

    button_fourth = tk.Button(fourth_frame['f'], text='Next', command=lambda: fifth_screen(fourth_frame['f'], fourth_frame["tresor_cache"]))
    button_fourth.pack()

def fifth_screen(*args):
    args[0].destroy()
    fifth_frame.insert(0, tk.Frame(window))
    f = fifth_frame[0]
    f.pack()
    tk.Label(f, text="Vous avez gagné " + args[1]).pack()


first_screen()
window.mainloop()

